'use strict';
module.exports = (sequelize, DataTypes) => {
  const Commentsancestors = sequelize.define('Commentsancestors', {
    CommentId: DataTypes.INTEGER,
    ancestorId: DataTypes.INTEGER
  }, {});
  Commentsancestors.associate = function(models) {
    // associations can be defined here
  };
  return Commentsancestors;
};