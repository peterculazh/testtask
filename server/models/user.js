'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    authId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  User.associate = function (models) {
    // associations can be defined here
    User.hasMany(models.Comment, {
      foreignKey: 'userId'
    });
  };
  return User;
};