const User = require("../models").User,
    passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy;

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findByPk(id, (err, user) => {})
        .then(user => {
            if (!user) return done("???", null);
            done(null, user);
        })
        .catch(err => {
            done(err, null);
        });
});

module.exports = function async (app, options) {
    if (!options.successRedirect)
        options.successRedirect = '/chat';
    if (!options.failureRedirect)
        options.failureRedirect = '/';

    return {
        init: function () {
            var env = app.get('env');
            var config = options.providers;
            passport.use(new FacebookStrategy({
                clientID: config.facebook[env].appId,
                clientSecret: config.facebook[env].appSecret,
                callbackURL: (options.baseUrl || '') + '/auth/facebook/callback',
            }, function (accessToken, refreshToken, profile, done) {
                var authId = 'facebook:' + profile.id;
                User.findOne({
                        where: {
                            authId: authId
                        }
                    })
                    .then(user => {
                        if (user) return done(null, user);
                        User.create({
                                authId: authId,
                                name: profile.displayName,
                                email: profile.emails,
                            })
                            .then(user => {
                                console.log(user);
                                if (user) return done(null, user);
                                return done("???", null);
                            })
                            .catch(err => {
                                return done(err, null);
                            });
                    })
                    .catch(err => {
                        return done(err, null);
                    });
            }));
            app.use(passport.initialize());
            app.use(passport.session());
        },

        registerRoutes: function () {
            app.post('/auth/facebook', (req, res, next) => {
                passport.authenticate('facebook')(req, res, next);
            });

            app.get('/auth/facebook/callback', passport.authenticate('facebook', {
                failureRedirect: options.failureRedirect
            }), (req, res) => {
                res.redirect(303, options.successRedirect);
            });
        }
    };
};