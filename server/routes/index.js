const messageController = require('../controllers').comment;
module.exports = (app) => {
    app.get('/', (req, res) => {
        res.render('index');
    });
    app.get('/chat', messageController.listAll);
    app.post('/chat/send', messageController.create);
    app.get('/logout', (req, res) => {
        req.session.destroy((err) => {
            if (err) return next(err);

            req.logout();

            res.redirect('/');
        });
    });
};