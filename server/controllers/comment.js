const User = require('../models').User;
const Comment = require("../models").Comment;
const sequelize = require('sequelize');
module.exports = {
    create(req, res) {
        var isAjaxRequest = req.xhr;
        var comment = {
            userId: req.user.id,
            ...req.body
        };
        if (comment.text !== '') {
            return Comment.create(comment)
                .then(item => {
                    if (isAjaxRequest) {
                        return res.status(201).send(item);
                    }
                    res.status(201).redirect('/chat');
                })
                .catch(err => {
                    if (isAjaxRequest) {
                        return res.status(400).send(err);
                    }
                    res.status(400).send(err);
                });
        }
    },

    listAll(req, res) {
        return Comment.findAll({
                hierarchy: true,
                include: [{
                    model: User
                }]

            })
            .then(items => {
                if (req.user) {
                    return res.status(201).render('chat', {
                        comments: items,
                        auth: true
                    });
                }
                res.status(201).render('chat', {
                    comments: items
                });
            })
            .catch(err => res.status(400).send(err));
    },

};