const path = require('path'),
    express = require('express'),
    Sequelize = require('sequelize');
require('sequelize-hierarchy')(Sequelize);
const cookieParser = require('cookie-parser'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    sequelizeStore = require('connect-session-sequelize')(session.Store),
    credentials = require("./credentials");
const handlebars = require("express-handlebars").create({
    defaultLayout: "main",
    partialsDir: [
        path.join(__dirname, './views/partials'),
    ]
});
var sequelize = new Sequelize('testtask', 'Petr I', 'root', {
    "dialect": "postgres"
}, );
sequelize.sync().then(() => {
    console.log("Database ready");
});
// (async function () {
//     await sequelize.models.Commentancestors.sync();
// })();
var myStore = new sequelizeStore({
    db: sequelize
});

const app = express();
// app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

var port = process.env.PORT || 3000;
const publicDirPath = path.join(__dirname + '/public');
app.use(express.static(publicDirPath));
app.use(cookieParser());
app.use(session({
    store: myStore,
    resave: true,
    secret: credentials.cookieSecret
}));
myStore.sync();
app.use(require('csurf')());
app.use(function (req, res, next) {
    res.locals._csrfToken = req.csrfToken();
    next();
});
var auth = require('./server/lib/auth')(app, {
    baseUrl: process.env.BASE_URL,
    providers: credentials.authProviders,
    successRedirect: '/chat',
    failureRedirect: '/'
});
auth.init();
auth.registerRoutes();


require('./server/routes')(app);

app.listen(port, () => {
    console.log("Server is up on port " + port);
});